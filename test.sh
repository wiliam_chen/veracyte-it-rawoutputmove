#!/bin/csh -f

#echo "test from mac"

if ($#argv != 1) then
        echo "usage: $0 <directory>"
        exit -1
endif
set dir=$1
echo "would execute arbitrary commands on directory $dir"
if ( -e $dir ) then
        echo "$dir exists"
else
        echo "$dir doesn't exist"
endif
