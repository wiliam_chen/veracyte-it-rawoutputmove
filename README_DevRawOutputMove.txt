Document DevRawOutputMove.yml

Usage: ansible-playbook DevRawOutputMove.yml

DevRawOutputMover host is Mendocino.veracyte.lan

The Ansible mover script makes a robust copy and moves files from the Raw Output directories safely.  It detects existing folders oin the dataset directory and ignores copying the files.  An rsync is first performed to a temporary directory to allow it to restart in the event of an interruption.  Once the rysnc is completed, after several minutes, a final move from the temporary directory is made into the final datasets directory.  A 2nd rsnyc is run to set the times on all the folders and subfolders.  The move is just a rename of the top level folder, and that happens quickly.  This allows the full completion of the move before the data is removed from the source.

DevRawOutputMove.yml
hosts
roles\DevRawOutputMove\tasks\main.yml
roles\DevRawOutputMove\vars\main.yml


Ansible
  | ++ DevRawOutputMove.yml
  | ++ hosts
  | __ roles
       |  __ DevRawOutputMove
             | __ tasks
                    | ++ main.yml
             | __ vars
                    | ++ main.yml

DevRawOutputMove.yml -- Copy of RawOutputMove.yml


